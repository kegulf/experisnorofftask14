**Experis Academy, Norway**

**Authors:**

* **Odd Martin Hansen**
* **Sondre Tofte**

# Task 14, Part 1 - Urgent! 
**This task takes precedence over all other tasks**

Write a program to solve the following problem:
A palindromic number reads the same both ways. The largest palindrome made from 
the product of two 2-digit numbers is 9009 = 91 × 99.

Find the largest palindrome made from the product of two 3-digit numbers.

Work in groups of 2-3. Come up with an approach before you start coding. Make 
sure you modularize accordingly, you do not have to use object orientation just get 
the result as soon as you can and enter it into project euler problem 4 to see if it 
is correct before you submit. Play ching chong cha to choose who does the coding 
while the others advise and help. Submit one solution per group. Put your names in the comments

*Due date: 16:00*

*Please don't stress, this is just a simulation. Pace yourselves and hand in what you 
have if you can't make the deadline.*


