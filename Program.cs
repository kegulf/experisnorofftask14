﻿using System;

namespace ExperisNoroffTask14 {
    class Program {
        static void Main(string[] args) {

            int largest = 0;
            int factor1 = 0;
            int factor2 = 0;

            Console.Write("Searching");

            long startTime = DateTimeOffset.Now.ToUnixTimeMilliseconds();


            Console.WriteLine();

            for (int i = 999; i >= 100; i--) {
                for (int j = i; j >= 100; j--) {

                    int product = i * j;

                    if (IsPalindrome(product)) {

                        // If the palindrome is bigger than the largest we've seen, save it
                        if(largest < product ) {
                            largest = product;
                            factor1 = j;
                            factor2 = i;
                        }
                    }
                }
            }

            Console.WriteLine($"The largest product of two 3-digit numbers that is a palindrom is:\n" +
                            $"{largest}\nThe factors of the number is:\n" +
                            $"{factor1} and {factor2}");
            
            long endTime = DateTimeOffset.Now.ToUnixTimeMilliseconds();

            Console.WriteLine($"Time spent searching {endTime - startTime} ms");
        }

        private static bool IsPalindrome(int palindromeCandidate) {

            // Convert to string for easier handling of digits.
            string numberString = $"{palindromeCandidate}";

            int length = numberString.Length;

            int center = (length % 2 == 0) ? length / 2 : length - 1 / 2;

            for (int i = 0; i < center; i++) 
                if( ! numberString[i].Equals( numberString[length - 1 - i]) ) 
                    return false;

            return true;

        }
    }
}
